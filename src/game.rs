use std::time::{Duration, SystemTime};

use sdl2::{VideoSubsystem, event::{Event}, mouse::MouseButton::{Left, Right}, keyboard::Keycode, keyboard::Mod};
use sdl2::video::Window;
use sdl2::render::Canvas;
use sdl2::rect::{Point, Rect};
use sdl2::pixels::Color;
use sdl2::{Sdl, EventPump};


struct Core {
    sdl_context: Sdl,
    video_subsystem: VideoSubsystem,
    canvas: Canvas<Window>
}

pub struct Game {
    core: Core,
    players: Vec<Player>,
}

struct Player {
    id: u8,
    username: String,
    color: Color,
    rect: Rect,
    selected: bool,
}

impl Core {
    fn new() -> Core {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();
        let window = video_subsystem
            .window("Funny Boxes", 800, 600)
            .position_centered()
            .build().unwrap();
        let canvas = window.into_canvas().build().unwrap();
        Core {
            sdl_context,
            video_subsystem,
            canvas,
        }
    }
}

impl Game {
    fn new(players_count: usize) -> Game {
        let mut players = Vec::with_capacity(players_count);
        for i in 0..players_count {
            let username = format!("Player-{}", i);
            players.push(Player::new(i as u8, username))
        }
        Game {
            core: Core::new(),
            players,
        }
    }

    fn add_player(&mut self, player: Player) {
        self.players.push(player)
    }

    fn draw_playground(&mut self) {
        self.core.canvas.set_draw_color(Color::RGB(0, 255, 255));
        self.core.canvas.clear();
    }

    fn draw_players(&mut self) {
        self.core.canvas.set_draw_color(Color::RGB(0, 0, 0));
        for player in self.players.iter() {
            self.core.canvas.draw_rect(player.rect).unwrap();
            if player.selected {
                self.core.canvas.set_draw_color(player.color);
                self.core.canvas.fill_rect(player.rect).unwrap();
                self.core.canvas.set_draw_color(Color::RGB(0, 0, 0));
            }
        }
        self.core.canvas.present()
    }

    fn select_players(&mut self, x: i32, y: i32) {
        for player in self.players.iter_mut() {
            if player.contain_point(x, y) {
                player.select()
            }
        }
    }

    fn move_left(&mut self) {
        for player in self.players.iter_mut() {
            if player.selected {
                let new_position = player.rect.x() - 5;
                if new_position >= 0 {
                    player.rect.set_x(new_position)
                } else {
                    player.rect.set_x(0)
                }
            }
        }
    }

    fn move_right(&mut self) {
        for player in self.players.iter_mut() {
            if player.selected {
                let new_position = player.rect.x() + 5;
                let max_right_position = self.core.canvas.window().size().0 as i32 - player.rect.w;
                if new_position <= max_right_position {
                    player.rect.set_x(new_position)
                } else {
                    player.rect.set_x(max_right_position)
                }
            }
        }
    }

    fn move_up(&mut self) {
        for player in self.players.iter_mut() {
            if player.selected {
                let new_position = player.rect.y() - 5;
                if new_position >= 0 {
                    player.rect.set_y(new_position)
                } else {
                    player.rect.set_y(0)
                }
            }
        }
    }

    fn move_down(&mut self) {
        for player in self.players.iter_mut() {
            if player.selected {
                let new_position = player.rect.y() + 5;
                let max_bottom_position = self.core.canvas.window().size().1 as i32 - player.rect.h;
                if new_position <= max_bottom_position {
                    player.rect.set_y(new_position)
                } else {
                    player.rect.set_y(max_bottom_position)
                }
            }
        }
    }

    fn release_all_players(&mut self) {
        for player in self.players.iter_mut() {
            player.release()
        }
    }

    fn events(&mut self) -> EventPump {
        self.core.sdl_context.event_pump().unwrap()
    }
}

impl Player {
    fn new(id: u8, username: String) -> Player {
        Player {
            id,
            username,
            color: Color::RGB(20 * id, 100, 150),
            rect: Rect::new(10, 10 + 55 * id as i32 , 50, 50),
            selected: false,
        }
    }

    fn contain_point(&self, x: i32, y: i32) -> bool {
        (self.rect.x() + self.rect.width() as i32) >= x && x >= self.rect.x() && (self.rect.y() + self.rect.height() as i32) >= y && y >= self.rect.y()
    }

    fn select(&mut self) {
        self.selected = true;
    }

    fn release(&mut self) {
        self.selected = false;
    }
}

pub fn init_game(players_count: usize) -> Game {
    Game::new(players_count)
}

pub fn game_loop(mut game: Game) {

    game.draw_playground();
    game.draw_players();

    let mut start_time = SystemTime::now();
    let mut frames = 0;

    'running: loop {
        game.draw_playground();

        for event in game.events().poll_iter() {
            match event {
                Event::Quit {..} |
                // Keyboard events
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                Event::KeyDown {keycode: Some(Keycode::Space), ..} => {
                    game.release_all_players()
                },
                Event::KeyDown {keycode: Some(Keycode::Left), repeat: true, ..} |
                Event::KeyDown {keycode: Some(Keycode::Left), ..} => {
                    game.move_left()
                }
                Event::KeyDown {keycode: Some(Keycode::Right), repeat: true, ..} |
                Event::KeyDown {keycode: Some(Keycode::Right), ..} => {
                    game.move_right()
                }
                Event::KeyDown {keycode: Some(Keycode::Down), repeat: true, ..} |
                Event::KeyDown {keycode: Some(Keycode::Down), ..} => {
                    game.move_down()
                }
                Event::KeyDown {keycode: Some(Keycode::Up), repeat: true, ..} |
                Event::KeyDown {keycode: Some(Keycode::Up), ..} => {
                    game.move_up()
                }
                // Mouse events
                Event::MouseMotion {xrel, yrel, ..} => {
                    // for player in game.players.iter_mut() {
                    //     if player.used {
                    //         player.rect.set_x(player.rect.x() + xrel);
                    //         player.rect.set_y(player.rect.y() + yrel);
                    //     }
                    // }
                },
                Event::MouseButtonDown {mouse_btn, x, y, ..} => {
                    match mouse_btn {
                        Left => {
                            game.select_players(x, y);
                        },
                        Right => {
                            // for obj in objects.iter_mut() {
                            //     if obj.is_inside(x, y) {
                            //         obj.change_color()
                            //     };
                            // }
                        },
                        _ => {},
                    }
                },
                Event::MouseButtonUp {mouse_btn, ..} => {
                    match mouse_btn {
                        Left => {
                            // for player in game.players.iter_mut() {
                            //     if player.used {
                            //         player.used = false;
                            //     }
                            // }
                        },
                        Right => println!("Right Mouse button up"),
                        _ => {},
                    }
                },
                _ => {}
            }
        }

        // The rest of the game loop goes here...
        game.draw_players();
        
        frames += 1;
        let end_time = SystemTime::now();
        if end_time.duration_since(start_time).unwrap() >= Duration::from_secs(1) {
            println!("FPS: {}", frames);
            frames = 0;
            start_time = end_time;
        }
    }
}
