mod game;

extern crate sdl2;

use game::{game_loop, init_game};


fn main() {
    let game = init_game(4);
    game_loop(game);
}
